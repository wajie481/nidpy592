#第一财经JJ租号捕鱼靠谱商人ciw
#### 介绍
JJ租号捕鱼靠谱商人【溦:844825】，JJ炮台租号商【溦:844825】，JJ租炮商人【溦:844825】，JJ诚信租号店铺【溦:844825】，JJ经典捕鱼号出租【溦:844825】，　　表叔的母亲去世得早。因为害怕余震，表叔的父亲就让表叔和另外两个小表叔和自己在一个屋里睡觉，以便在地震发生时一声令下往屋外跑。就在没搬进屋里几天后的一个晚上，熟睡中的表叔被一块从墙壁上掉下来的山芋砸醒，他马上意识到是余震发生了，一跃而起，高喊一声，向着有着朦胧亮光的窗户冲去，整个光溜溜的身子从窗玻璃中钻出，重重地摔在外面院子的地上。表叔的父亲和两个小表叔被喊声惊醒，拉着电灯，瞧瞧并没有什么地震，却发现窗子玻璃粉碎，满身血淋林的表叔正站在院里发呆。表叔的父亲来到院里，看看表叔的惨相，又看看站在身旁的两个孩子，又恨又怒的气色升腾起来，对着表叔的脸上啐了一口。后来，我听到大人们都在说，张家的大小子真不是东西，知道地震了只知道自己往外跑，连他爹和兄弟们都不叫一声，生这儿子有什么用，真不如当时一屁股把他坐死，免得今天让人们看笑话。从此我便很少看到表叔了，偶尔看到，表叔也是把那还透着血鳞子的脸扭过去，不再理我。父母亲告诉我，以后不要再找表叔去玩，为什么，他们没说。随后，我们在一起玩儿的一群孩子就散了。直到上初中上高中，仍有别的村的同学问我，你们村是不是有个倍混蛋的小子，知道地震了只顾自己往外跑。我无言以对。
　　一个月，给我的感觉是短暂的，这个月换发会计证，工作很忙，不过忙也有忙的好处，埋头工作能够让人忽略一切，再仰首时，时光便一恍而去了。而在母亲那里，便会觉得很漫长了，一是有我不回家的原故，母亲冷清；二是苇受脚伤，也有无尽的牵挂在里面。母亲经常在晚上打来电话，嘘长问短，有时我刚打开电脑酝酿情绪写作，母亲的电话却在这时打了过来，只好弃开电脑去接听电话。现在终于能够一周回一次家了，母亲的脸上带着微笑，很宽慰的样子。

#### 参与贡献
Fork 本仓库
新建 Feat_xxx 分支
提交代码
新建 Pull Request
#### 特技
使用 Readme_XXX.md 来支持不同的语言，例如 Readme_en.md, Readme_zh.md
Gitee 官方博客 blog.gitee.com
你可以 https://gitee.com/explore 这个地址来了解 Gitee 上的优秀开源项目
GVP 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
Gitee 官方提供的使用手册 https://gitee.com/help
Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 https://gitee.com/gitee-stars/